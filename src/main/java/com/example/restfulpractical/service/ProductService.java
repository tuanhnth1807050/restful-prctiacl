package com.example.restfulpractical.service;

import com.example.restfulpractical.entity.Product;
import com.example.restfulpractical.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    final
    ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product sellProduct(Long productId, Integer quantity) {
        Optional<Product> resultProduct = productRepository.findById(productId);
        if (resultProduct.isPresent()) {
            Product soldProduct = resultProduct.get();
            soldProduct.quantity = soldProduct.quantity - quantity;
            return productRepository.save(soldProduct);
        } else {
            return null;
        }
    }
}
