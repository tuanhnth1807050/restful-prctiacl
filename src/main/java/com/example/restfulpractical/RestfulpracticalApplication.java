package com.example.restfulpractical;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulpracticalApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfulpracticalApplication.class, args);
    }

}
