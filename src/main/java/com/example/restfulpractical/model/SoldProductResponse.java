package com.example.restfulpractical.model;

import lombok.Data;

@Data
public class SoldProductResponse {
    public Long id;
    public String name;
    public Double price;
    public Integer quantity;
    public Double totalPrice;
}
