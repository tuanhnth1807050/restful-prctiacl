package com.example.restfulpractical.controller;

import com.example.restfulpractical.entity.Product;
import com.example.restfulpractical.mapper.ProductMapper;
import com.example.restfulpractical.model.SoldProductResponse;
import com.example.restfulpractical.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/products")
public class ProductController {
    final
    ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(path = "")
    public ResponseEntity getAllProducts() {
        try {
            List<Product> products = productService.getAllProducts();
            if (!products.isEmpty()) {
                return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("No product found", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "")
    public ResponseEntity addProduct(@RequestBody Product product) {
        try {
            Product newlyCreatedProduct = productService.addProduct(product);
            return new ResponseEntity<>(newlyCreatedProduct, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "")
    public ResponseEntity sellProduct(@RequestParam Long productId, @RequestParam Integer quantity) {
        try {
            Product product = productService.sellProduct(productId, quantity);
            if (product != null) {
                SoldProductResponse soldProductResponse = ProductMapper.INSTANCE.toSoldProductResponse(product);
                soldProductResponse.totalPrice = soldProductResponse.price * quantity;
                return new ResponseEntity<>(soldProductResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Can not updated", HttpStatus.NOT_MODIFIED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
