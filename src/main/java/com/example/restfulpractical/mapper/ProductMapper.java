package com.example.restfulpractical.mapper;

import com.example.restfulpractical.entity.Product;
import com.example.restfulpractical.model.SoldProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    SoldProductResponse toSoldProductResponse(Product product);
}
